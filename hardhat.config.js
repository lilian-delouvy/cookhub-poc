require("@nomiclabs/hardhat-waffle");
const fs = require("fs");
const projectId = fs.readFileSync(".project-id-secret").toString();
const privateKey = fs.readFileSync(".private-key-secret").toString();

module.exports = {
  networks: {
    hardhat: {
      chainId: 1337
    },
    mumbai: {
      url: `https://polygon-mumbai.infura.io/v3/${projectId}`,
      accounts: [privateKey]
    },
    mainnet: {
      url: `https://polygon-mainnet.infura.io/v3/${projectId}`,
      accounts: [privateKey]
    }
  },
  solidity: "0.8.4",
};
