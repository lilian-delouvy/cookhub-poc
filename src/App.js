import {HashRouter} from "react-router-dom";
import Routes from "./Routes";

function App() {
  return (
      <HashRouter basename={window.location.pathname}>
          <Routes/>
      </HashRouter>
  );
}

export default App;
