import React from "react";
import './FadedText.css';

const FadedText = ({text}) => {
    return(
        <div className="faded-text">{text}</div>
    )
};

export default FadedText;