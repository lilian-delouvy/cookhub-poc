import {Switch, Route} from 'react-router';
import HomePage from "./Pages/HomePage";
import RecipesCreator from "./Pages/RecipesCreator";
import RecipesMarket from "./Pages/RecipesMarket";
import RecipesInventory from "./Pages/RecipesInventory";
import NotFound from "./NotFound";

const Routes = () => {

    return (
        <Switch>
            <Route exact path="/">
                <HomePage/>
            </Route>
            <Route exact path="/recipe-creation">
                <RecipesCreator/>
            </Route>
            <Route exact path="/recipes-market">
                <RecipesMarket/>
            </Route>
            <Route exact path="/recipes-inventory">
                <RecipesInventory/>
            </Route>
            <Route>
                <NotFound/>
            </Route>
        </Switch>
    )

};

export default Routes;