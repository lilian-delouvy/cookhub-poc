﻿import {Link} from "@mui/material";
import './NavBar.scss';

const NavBar = () => {
    
    return(
        <div className="navbar">
            <Link href="#" underline="none" color="white">
                CookHub
            </Link>
            <Link href="#recipes-market" underline="none" color="white">
                Marché
            </Link>
            <Link href="#recipe-creation" underline="none" color="white">
                Créer ma recette !
            </Link>
            <Link href="#recipes-inventory" underline="none" color="white">
                Mes recettes
            </Link>
        </div>
    )
    
};

export default NavBar;