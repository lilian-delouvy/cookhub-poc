import {useEffect, useState} from "react";
import { ethers } from 'ethers';
import axios from 'axios';
import './RecipesMarket.scss';

import {nftAddress, nftMarketAddress} from "../../config";

import NFT from '../../artifacts/contracts/RecipeNFT.sol/RecipeNFT.json';
import Market from '../../artifacts/contracts/RecipeMarket.sol/RecipeMarket.json';
import NavBar from "../NavBar/NavBar";
import {Button} from "@mui/material";
import Web3Modal from "web3modal";

const RecipesMarket = () => {
    
    const [state, setState] = useState({
        items: [],
        isLoading: true
    });
    
    useEffect(() => {
        getNFTArray().then(() => console.log("NFT recovered !"));
    }, []);
    
    const getNFTArray = async () => {
        const provider = new ethers.providers.JsonRpcProvider();
        const contract = new ethers.Contract(nftAddress, NFT.abi, provider);
        const marketContract = new ethers.Contract(nftMarketAddress, Market.abi, provider);
        const data = await marketContract.getRecipeMarketItems();
        
        const items = await Promise.all(
            data.map(async current => {
                const tokenURI = await contract.tokenURI(current.tokenId);
                const metadata = await axios.get(tokenURI);
                const price = ethers.utils.formatUnits(current.price.toString(), 'ether');
                return {
                    price,
                    tokenId: current.tokenId.toNumber(),
                    seller: current.seller,
                    owner: current.owner,
                    image: metadata.data.url,
                    name: metadata.data.name,
                    description: metadata.data.description
                }
            })
        );
        console.log(items);
        setState({...state, items, isLoading: false});
    }

    const buyNFT = async (nft) => {
        const web3modal = new Web3Modal();
        const connection = await web3modal.connect();
        const signer = new ethers.providers.Web3Provider(connection).getSigner();
        const contract = new ethers.Contract(nftMarketAddress, Market.abi, signer);

        const price = ethers.utils.parseUnits(nft.price.toString(), 'ether');
        const transaction = await contract.createSale(nft.tokenId, nftAddress, {value: price});
        await transaction.wait();
        await getNFTArray();
    };
    
    return(
        <>
            <NavBar/>
            {state.isLoading &&
                <div className="recipe-market__message">Chargement...</div>
            }
            {(!state.isLoading && !state.items.length) &&
                <div className="recipe-market__message">Aucun NFT n'est en vente !</div>
            }
            <div className="recipe-market">
                {state.items.map((nft, index) => (
                    <div className="nft__container" key={index}>
                        <img className="nft__image" src={nft.image} alt={nft.name}/>
                        <p className="nft__text">{nft.name}</p>
                        <p className="nft__text">{nft.description}</p>
                        <p className="nft__text">Prix: {nft.price} ether</p>
                        <Button onClick={() => buyNFT(nft)}>Acheter</Button>
                    </div>
                ))}
            </div>
        </>
    )
};

export default RecipesMarket;