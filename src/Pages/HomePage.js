import logo from "../cookhub-logo.svg";
import FadedText from "../Text/FadedText";
import './HomePage.scss';
import NavBar from "../NavBar/NavBar";

const HomePage = () => {
    return(
        <>
            <NavBar/>
            <div className="homepage">
                <img src={logo} className="homepage__logo" alt="logo" />
                <h1>CookHub</h1>
                <div className="homepage__content">
                    <FadedText text="Ceci est le POC de CookHub, qui met en avant l'utilisation des NFT dans le processus de gestion des recettes."/>
                </div>
            </div>
        </>
    )
};

export default HomePage;