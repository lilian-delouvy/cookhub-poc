import NavBar from "../NavBar/NavBar";
import './RecipesCreator.scss';
import {Button, MenuItem, Select} from "@mui/material";
import {useState} from "react";
import { create } from 'ipfs-http-client';
import Web3Modal from "web3modal";
import { ethers } from 'ethers';
import NFT from '../../artifacts/contracts/RecipeNFT.sol/RecipeNFT.json';
import Market from '../../artifacts/contracts/RecipeMarket.sol/RecipeMarket.json';
import { nftAddress, nftMarketAddress } from '../../config';
import {Redirect} from "react-router";
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import './RecipesCreator.scss';

const client = create('https://ipfs.infura.io:5001/api/v0');

const optionsSelect = [
    {value: 0, label: "Étape sans ingrédients"},
    {value: 1, label: "Étape avec ingrédients"}
]

const RecipesCreator = () => {
    
    const [state, setState] = useState({
        name: "",
        description: "",
        url: "",
        price: "",
        isCreated: false,
        steps: [],
        currentSelect: optionsSelect[0]
    });
    
    const onFileInserted = async (e) => {
        const insertedFile = e.target.files[0];
        try{
            const addedImage = await client.add(insertedFile, {progress: (progress) => console.log(`received: ${progress}`)});
            const imageUrl = `https://ipfs.infura.io/ipfs/${addedImage.path}`;
            setState({...state, url: imageUrl});
        } catch (error) {
            console.log(error);
        }
    }
    
    const createRecipe = async () => {
        if(!state.name || !state.steps.length || !state.url || state.price === 0){
            return;
        }
        const name = state.name;
        console.log(state.steps);
        const description = JSON.stringify(state.steps);
        const url = state.url;
        const price = state.price;
        const itemString = JSON.stringify({name, description, url, price});
        console.log(description);
        try {
            const addedItem = await client.add(itemString);
            const url = `https://ipfs.infura.io/ipfs/${addedItem.path}`;
            await sellNFT(url);
        } catch(error){
            console.log(error);
        }
    }
    
    const sellNFT = async (imageURL) => {
        const web3modal = new Web3Modal();
        const connection = await web3modal.connect();
        const signer = new ethers.providers.Web3Provider(connection).getSigner();
        
        let contract = new ethers.Contract(nftAddress, NFT.abi, signer);
        let transaction = await contract.createToken(imageURL);
        let finishedTransaction = await transaction.wait();
        
        let event = finishedTransaction.events[0];
        let value = event.args[2];
        let tokenId = value.toNumber();
        const price = ethers.utils.parseUnits(state.price, 'ether');
        contract = new ethers.Contract(nftMarketAddress, Market.abi, signer);
        const listingPrice = await contract.getListingPrice();
        const stringListingPrice = listingPrice.toString();
        
        transaction = await contract.createRecipeMarketItem(
            tokenId,
            nftAddress,
            price,
            {value: stringListingPrice}
        );
        
        await transaction.wait();
        setState({...state, isCreated: true});
    }
    
    const addStep = () => {
        const currentSteps = state.steps;
        state.currentSelect.value === 0 ? 
            currentSteps.push({description: ""})
        :
            currentSteps.push({description: "", quantity: 0});
        setState({...state, steps: currentSteps});
    }
    
    const updateStep = (index, isDesc, newValue) => {
        const tempSteps = state.steps;
        isDesc ? tempSteps[index].description = newValue : tempSteps[index].quantity = newValue;
        setState({...state, steps: tempSteps});
    }
    
    return(
        <>
            {state.isCreated &&
                <Redirect to="/"/>
            }
            <NavBar/>
            <div className="recipes-creator">
                <h2>Création de recette !</h2>
                <div className="recipes-creator__item">
                    <label className="recipes-creator__input-label" htmlFor="text-name">Nom</label>
                    <input className="recipes-creator__input" id="text-name" type="text" onChange={(e) => setState({...state, name: e.currentTarget.value})}/>
                </div>
                <div className="recipes-creator__item">
                    <Select
                        id="recipes-step-select"
                        value={state.currentSelect.value}
                        label={state.currentSelect.label}
                        onChange={(e) => setState({...state, currentSelect: optionsSelect[e.target.value]})}
                        sx={{color: "white", border: "1px solid white"}}
                    >
                        <MenuItem value={optionsSelect[0].value}>{optionsSelect[0].label}</MenuItem>
                        <MenuItem value={optionsSelect[1].value}>{optionsSelect[1].label}</MenuItem>
                    </Select>
                    <Button onClick={addStep}>Ajouter</Button>
                </div>
                {state.steps.length !== 0 &&
                    <div className="recipes-creator__item">
                        {state.steps.map((step, index) => (
                            <div key={index} className="recipes-creator__step">
                                <label className="recipes-creator__input-label recipes-creator__input--inline" htmlFor={`step-${index}-description`}>
                                    Description de l'étape {index} :
                                    <input className="recipes-creator__input recipes-creator__input--inline" type="text" onChange={(e) => updateStep(index, true, e.currentTarget.value)}/>
                                </label>
                                {step.hasOwnProperty("quantity") &&
                                    <label className="recipes-creator__input-label" htmlFor={`step-${index}-quantity`}>
                                        Quantité (en grammes):
                                        <input className="recipes-creator__input recipes-creator__input--quantity recipes-creator__input--inline" type="number" onChange={(e) => updateStep(index, false, e.currentTarget.valueAsNumber)}/>
                                    </label>
                                }
                            </div>
                        ))}
                    </div>
                }
                <div className="recipes-creator__item">
                    <label className="recipes-creator__input-label" htmlFor="number-price">Prix</label>
                    <input className="recipes-creator__input" type="number" onChange={(e) => setState({...state, price: e.currentTarget.value})}/>
                </div>
                <label htmlFor="file-upload" className="recipes-creator__input-item">
                    <CloudUploadIcon/> Image de la recette
                </label>
                <input id="file-upload" type="file" name="image_recette" onChange={onFileInserted}/>
                {state.url &&
                    <div className="recipes-creator__image-container">
                        <img className="recipes-creator__image" src={state.url} alt="recipe_image"/>
                    </div>
                }
                <div className="recipes-creator__item recipes-creator__item--button">
                    <Button onClick={() => createRecipe()}>Créer !</Button>
                </div>
            </div>
        </>
    )
};

export default RecipesCreator;