import NavBar from "../NavBar/NavBar";
import {ethers} from 'ethers';
import {useEffect, useState} from 'react';
import axios from 'axios';
import Web3Modal from "web3modal";
import './RecipesInventory.scss';

import {nftAddress, nftMarketAddress} from '../../config';
import Market from '../../artifacts/contracts/RecipeMarket.sol/RecipeMarket.json';
import NFT from '../../artifacts/contracts/RecipeNFT.sol/RecipeNFT.json';

const RecipesInventory = () => {
    
    const [state, setState] = useState({
        items: [],
        isLoading: true
    });
    
    useEffect(() => {
        getUserItems();
    }, []);
    
    const getUserItems = async () => {
        const web3modal = new Web3Modal({
            network: "mainnet",
            cacheProvider: true
        });
        const connection = await web3modal.connect();
        const provider = new ethers.providers.Web3Provider(connection)
        const signer = provider.getSigner();
        
        const marketContract = new ethers.Contract(nftMarketAddress, Market.abi, signer);
        const tokenContract = new ethers.Contract(nftAddress, NFT.abi, provider);
        const nfts = await marketContract.getItemsPurchasedByMsgSender();
        
        const nftItems = await Promise.all(nfts.map(async nft => {
            const tokenUri = await tokenContract.tokenURI(nft.tokenId);
            const metadata = await axios.get(tokenUri);
            const price = ethers.utils.formatUnits(nft.price.toString(), 'ether');
            return {
                price,
                name: metadata.data.name,
                tokenId: nft.tokenId.toNumber(),
                seller: nft.seller,
                owner: nft.owner,
                image: metadata.data.url,
                description: metadata.data.description
            };
        }));
        setState({...state, items: nftItems, isLoading: false});
    }
    
    return(
        <>
            <NavBar/>
            {state.isLoading &&
                <div className="recipes-inventory__message">Chargement...</div>
            }
            {(!state.isLoading && !state.items.length) &&
                <div className="recipes-inventory__message">Vous ne possédez aucun NFT !</div>
            }
            <div className="recipes-inventory">
                {state.items.map((nft, index) => (
                    <div className="nft__container" key={index}>
                        <img className="nft__image" src={nft.image} alt={nft.name}/>
                        <p className="nft__text">{nft.name}</p>
                        <p className="nft__text">{nft.description}</p>
                        <p className="nft__text">Price: {nft.price} ether</p>
                    </div>
                ))}
            </div>
        </>
    )
};

export default RecipesInventory;