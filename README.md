# CookHub Proof Of Concept

This project is a POC for CookHub.

## What is CookHub ?

CookHub is an application which will allow users to interact and talk about batch cooking practice.

To do so, the users will be able to:
- create recipes (and put them as NFT tokens or not)
- sell and buy recipes made with NFT tokens
- select recipes
- create a batch cooking session with them
- have an algorithm optimize the session for them to improve time management
- interact with other users by sharing recipes and reviewing them
- challenge other users in competitive sessions:
  - two users will have to make a recipe and a third-party user will review who was the most successful at it. The winner will get a prize and the third-party user will get a compensation.
- generate a shopping list out of the selected recipes

There will also be an in-app currency which users can trade in exchange for promotion coupons in participating stores.

## On what does this POC focuses on ?

The goal of this POC is to show how the NFT part of the project is handled.

To do so, this POC displays the following features:
- recipe creation
- recipe browsing
- sell a recipe
- buy a recipe
- highlight the nft transaction


## How to use this POC ?

Clone the project : https://gitlab.com/lilian-delouvy/cookhub-poc.git
```shell
cd cookhub-poc
```

You will have to run three commands, each one of them in a dedicated terminal:

To run the front-end of the project, please run :
```shell
npm start
```

You will need MetaMask extension to handle wallets.

To create hardhat node, run :
```shell
npx hardhat node
```

You can select two private keys out of the displayed keys and add them to MetaMask. The first one will be used as the owner's account and the second one as the buyer's account.

In order to deploy the project, you will have to run (the hardhat node has to be running first) :
```shell
npx hardhat run scripts/deploy.js --network localhost
```

If you want to run the unit test which creates two market items and executes a sale, you can also use the following:
```shell
npx hardhat test
```

## WARNING !

Please note that the project won't work "as-is", because the source code requires parameters that are not on github for safety reasons.

If you want to use this project, please get in touch with the project maintainers to get help on the setup procedure.

The required parameters are:
- the project id, which is received from a ".project-id-secret" file
- the private key, which refers to the user's wallet private key and is stored in a ".private-key-secret" file
- the ".config" file, which contains the adresses of the nft and the nft market

## CONTRIBUTION

This project was made as a classroom project by Richard Deleplanque, Lilian Delouvy and Raphaël Dumortier.
