const { ethers } = require("hardhat");

describe("NFTMarket", function() {
    it("Should create and execute market sales", async function() {
        /* deploy the marketplace */
        const Market = await ethers.getContractFactory("RecipeMarket");
        const market = await Market.deploy();
        await market.deployed();
        const marketAddress = market.address;

        /* deploy the NFT contract */
        const NFT = await ethers.getContractFactory("RecipeNFT");
        const nft = await NFT.deploy(marketAddress);
        await nft.deployed();
        const nftContractAddress = nft.address;

        let listingPrice = await market.getListingPrice();
        listingPrice = listingPrice.toString();

        const auctionPrice = ethers.utils.parseUnits('1', 'ether');

        /* create two tokens and put them for sale */
        await nft.createToken("https://www.mytokenlocation.com");
        await nft.createToken("https://www.mytokenlocation2.com");

        await market.createRecipeMarketItem(1, nftContractAddress, auctionPrice, { value: listingPrice });
        await market.createRecipeMarketItem(2, nftContractAddress, auctionPrice, { value: listingPrice });

        const [_, buyerAddress] = await ethers.getSigners();

        /* execute sale of token to another user */
        await market.connect(buyerAddress).createSale(1, nftContractAddress, { value: auctionPrice});

        /* query for and return the unsold items */
        let items = await market.getRecipeMarketItems();
        items = await Promise.all(items.map(async i => {
            const tokenUri = await nft.tokenURI(i.tokenId);
            let item = {
                price: i.price.toString(),
                tokenId: i.tokenId.toString(),
                seller: i.seller,
                owner: i.owner,
                tokenUri
            };
            return item;
        }))
        console.log('items: ', items);
    })
})