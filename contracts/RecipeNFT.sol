// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract RecipeNFT is ERC721URIStorage {
    using Counters for Counters.Counter;
    Counters.Counter private _nbTokens;
    address marketAddress;

    constructor(address marketPlaceAddress) ERC721("Metaverse Tokens", "METT") {
        marketAddress = marketPlaceAddress;
    }

    function createToken(string memory tokenURI) public returns (uint) {
        _nbTokens.increment();
        uint256 newId = _nbTokens.current();

        _mint(msg.sender, newId);
        _setTokenURI(newId, tokenURI);
        setApprovalForAll(marketAddress, true);
        return newId;
    }
}