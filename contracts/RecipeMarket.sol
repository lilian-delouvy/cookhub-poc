// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

import "./RecipeMarketItem.sol";

contract RecipeMarket is ReentrancyGuard {
    using Counters for Counters.Counter;
    Counters.Counter private _nbItems;
    Counters.Counter private _nbItemsSold;
    uint256 listingPrice = 0.02 ether;
    address payable owner;
    mapping(uint256 => RecipeMarketItem) private recipeMarketItemFromId;

    constructor() {
        owner = payable(msg.sender);
    }

    event onRecipeMarketItemCreation (
        uint indexed itemId,
        uint256 indexed tokenId,
        address indexed contractAddress,
        address seller,
        address owner,
        uint256 price,
        bool isSold
    );

    function getListingPrice() public view returns (uint256) {
        return listingPrice;
    }

    function getRecipeMarketItems() public view returns (RecipeMarketItem[] memory){
        uint nbItems = _nbItems.current();
        uint nbAvailableItems = _nbItems.current() - _nbItemsSold.current();

        RecipeMarketItem[] memory returnedItems = new RecipeMarketItem[](nbAvailableItems);
        uint index = 0;
        for(uint i = 0; i < nbItems; i++){
            if(recipeMarketItemFromId[i + 1].owner == address(0)) {
                uint current = recipeMarketItemFromId[i + 1].itemId;
                RecipeMarketItem storage item = recipeMarketItemFromId[current];
                returnedItems[index] = item;
                index += 1;
            }
        }
        return returnedItems;
    }

    function getItemsCreatedByMsgSender() public view returns (RecipeMarketItem[] memory) {
        uint nbItems = _nbItems.current();
        uint nbSenderItems = 0;

        for(uint i = 0; i < nbItems; i++){
            if(recipeMarketItemFromId[i + 1].seller == msg.sender) {
                nbSenderItems += 1;
            }
        }

        RecipeMarketItem[] memory returnedItems = new RecipeMarketItem[](nbSenderItems);
        uint index = 0;
        for(uint i = 0; i < nbItems; i++){
            if(recipeMarketItemFromId[i + 1].seller == msg.sender){
                uint current = i + 1;
                RecipeMarketItem storage currentItem = recipeMarketItemFromId[current];
                returnedItems[index] = currentItem;
                index += 1;
            }
        }
        return returnedItems;
    }

    function getItemsPurchasedByMsgSender() public view returns(RecipeMarketItem[] memory) {
        uint nbItems = _nbItems.current();
        uint nbSenderItems = 0;

        for(uint i = 0; i < nbItems; i++){
            if(recipeMarketItemFromId[i + 1].owner == msg.sender){
                nbSenderItems += 1;
            }
        }

        RecipeMarketItem[] memory returnedItems = new RecipeMarketItem[](nbSenderItems);
        uint index = 0;
        for(uint i = 0; i < nbItems; i++){
            if(recipeMarketItemFromId[i + 1].owner == msg.sender){
                uint current = i + 1;
                RecipeMarketItem storage currentItem = recipeMarketItemFromId[current];
                returnedItems[index] = currentItem;
                index += 1;
            }
        }
        return returnedItems;
    }

    function createRecipeMarketItem(uint256 tokenId, address contractAddress, uint256 price) public payable nonReentrant {
        require(price > 0, "Price must be greater than 0 !");
        require(msg.value == listingPrice, "User sending this transaction has to provide the required listing price !");

        _nbItems.increment();
        uint256 currentItemId = _nbItems.current();

        recipeMarketItemFromId[currentItemId] = RecipeMarketItem(
            currentItemId,
            tokenId,
            contractAddress,
            payable(msg.sender),
            payable(address(0)),
            price,
            false
        );

        IERC721(contractAddress).transferFrom(msg.sender, address(this), tokenId);

        emit onRecipeMarketItemCreation(
            currentItemId,
            tokenId,
            contractAddress,
            msg.sender,
            address(0),
            price,
            false
        );
    }

    function createSale(uint256 itemId, address contractAddress) public payable nonReentrant {
        uint tokenId = recipeMarketItemFromId[itemId].tokenId;
        uint price = recipeMarketItemFromId[itemId].price;

        require(msg.value == price, "Please pay the requested price to purchase the item");

        recipeMarketItemFromId[itemId].seller.transfer(msg.value);
        IERC721(contractAddress).transferFrom(address(this), msg.sender, tokenId);
        recipeMarketItemFromId[itemId].owner = payable(msg.sender);
        recipeMarketItemFromId[itemId].isSold = true;
        _nbItemsSold.increment();
        payable(owner).transfer(listingPrice);
    }

}

