struct RecipeMarketItem {
    uint itemId;
    uint256 tokenId;
    address contractAddress;
    address payable seller;
    address payable owner;
    uint256 price;
    bool isSold;
}