const hre = require("hardhat");

async function main() {
    const NFTMarket = await hre.ethers.getContractFactory("RecipeMarket");
    const NFT = await hre.ethers.getContractFactory("RecipeNFT");
    const nftMarket = await NFTMarket.deploy();
    const nft = await NFT.deploy(nftMarket.address);

    await nftMarket.deployed();
    await nft.deployed();

    console.log("recipeMarket deployed to:", nftMarket.address);
    console.log("recipeNft deployed to:", nft.address);
}

main()
    .then(
        () => process.exit(0)
    )
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
